'use strict';
'require form';
'require fs';
'require poll';
'require uci';
'require view';

return view.extend({
  render: function(data) {
    let self = this;
    let m, s, o;

    m = new form.Map('tailscale', _('TailScale'));
    m.anonymous = true;

    s = m.section(form.NamedSection, 'settings');

    o = s.option(form.DummyValue, '_status', _('Status'));
    o.render = function(section_id) {
      let status_div = E('div', { 'class': 'cbi-value-field' }, _('Loading...'));

      poll.add(function() {
        L.resolveDefault(fs.exec('/usr/sbin/tailscale', ['status', '--json']), {}).then(function(status) {
          try {
            status = JSON.parse(status.stdout);
            let text = self.getStatusText(status);

            if (status.BackendState === 'NeedsLogin' && !!status.AuthURL) {
              status_div.replaceChildren(E('a', { 'href': status.AuthURL, 'target': '_new' }, text));
            } else {
              status_div.innerText = text;
            }
          } catch {
            status_div.innerText = _('NOT RUNNING');
          }
        });
      });

      return E('div', { 'class': 'cbi-value' }, [
        E('label', { 'class': 'cbi-value-title' }, _('Status')),
        status_div
      ]);
    };

    return m.render();
  },

  getStatusText: function(status) {
    if (!status.BackendState) {
      return _('NOT RUNNING');
    }

    if (status.BackendState === 'NeedsLogin') {
      return _('NEEDS LOGIN');
    }

    if (status.BackendState === 'Starting') {
      return _('STARTING');
    }

    if (status.BackendState === 'Running') {
      return _('RUNNING');
    }

    return _('UNKNOWN');
  },
});
